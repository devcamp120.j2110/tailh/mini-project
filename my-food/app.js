// Select Element
let hero = document.querySelector(".hero");
let slider = document.querySelector(".slider");
let slides = slider.querySelectorAll(".slide");
let slideIndex = 0;
let canSlidePlay = true;
let hero_bgs = [
  "./assets/images/goi-cuon-bg.jpg",
  "./assets/images/pho-ga-bg.jpg",
  "./assets/images/bun-bo-bg.jpg",
];
let mobileMenu = document.querySelector("#mobile-menu-toggle");

// Handle Event
const showSlide = (index) => {
  slides.forEach((event) => {
    event.classList.remove("active");
  });
  slides[index].classList.add("active");
  hero.style.backgroundImage = `url(${hero_bgs[slideIndex]})`;
};

const nextSlide = () => {
  if (slideIndex + 1 === slides.length) {
    slideIndex = 0;
  } else {
    slideIndex += 1;
  }
  showSlide(slideIndex);
};

// pause slide when mouse come in slider
slider.addEventListener("mousemove", () => {
  canSlidePlay = false;
});
// resume slide when leave out slider
slider.addEventListener("mouseleave", () => {
  canSlidePlay = true;
});

showSlide(slideIndex);

//auto play slide
setInterval(() => {
  if (!canSlidePlay) return;
//   nextSlide();
}, 4000);

// Select Slide
document.querySelectorAll(".slider-control-item").forEach((item, index) => {
  item.addEventListener("click", () => {
    showSlide(index);
  });
});

// Open/Close Mobile Menu
mobileMenu.addEventListener("click", () => {
  document.querySelector("#main-menu").classList.toggle("active");
});

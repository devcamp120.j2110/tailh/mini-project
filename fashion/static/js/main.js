let productIndex = 0;
let productInfos = document.querySelectorAll(".product-info");
setTimeout(() => {
  productInfos[productIndex].classList.add("active");
}, 200);

// Slider
let isSliding = false;

const slide = () => {
  if (isSliding) return;
  isSliding = true;
  //Slide Product Info
  let currProduct = document.querySelector(".product-info.active");
  currProduct.classList.remove("active");
  // Set active product info
  if (productIndex + 1 > productInfos.length - 1) {
    productIndex = 0;
  } else {
    productIndex += 1;
  }
  productInfos[productIndex].classList.add("active");
  //Slide IMG
  let slider = document.querySelector(".slider"); //Selector Element
  let images = document.querySelectorAll(".slide"); //Selector Element
  let reverseList = Array.from(images).slice().reverse();
  let left = reverseList[0].offsetLeft + "px";
  let height = reverseList[0].offsetHeight + "px";
  let width = reverseList[0].offsetWidth + "px";
  let zIndex = reverseList[0].style.zIndex;

  reverseList.forEach((element, index) => {
    if (index < images.length - 1) {
      element.style.left = reverseList[index + 1].offsetLeft + "px";
      element.style.height = reverseList[index + 1].offsetHeight + "px";
      element.style.width = reverseList[index + 1].offsetWidth + "px";
      element.style.zIndex = reverseList[index + 1].style.zIndex;
      element.style.transform = "unset";
      element.style.opacity = "1";
    }
    if (index === images.length - 1) {
      element.style.transform = "scale(1.5)";
      element.style.opacity = "0";
      let clone = element.cloneNode(true);
      setTimeout(() => {
        element.remove();
        clone.style.transform = "scale(0)";
        clone.style.left = left;
        clone.style.height = height;
        clone.style.width = width;
        clone.style.opacity = "0";
        clone.style.zIndex = zIndex;
        clone.style.animation = "unset";

        slider.appendChild(clone);

        isSliding = false;
      }, 1000);
    }
  });
};

let sliderControl = document.querySelector(".slider-control");

sliderControl.onclick = () => {
  slide();
};

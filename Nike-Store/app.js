// Products
const products = [
  {
    id: 1,
    title: "Air Force",
    price: 119,
    colors: [
      {
        code: "black",
        img: "./assets/image/air.png",
      },
      {
        code: "darkblue",
        img: "./assets/image/air2.png",
      },
    ],
  },
  {
    id: 2,
    title: "Air Jordan",
    price: 149,
    colors: [
      {
        code: "lightgray",
        img: "./assets/image/jordan.png",
      },
      {
        code: "green",
        img: "./assets/image/jordan2.png",
      },
    ],
  },
  {
    id: 3,
    title: "Blazer",
    price: 109,
    colors: [
      {
        code: "lightgray",
        img: "./assets/image/blazer.png",
      },
      {
        code: "green",
        img: "./assets/image/blazer2.png",
      },
    ],
  },
  {
    id: 4,
    title: "Crater",
    price: 129,
    colors: [
      {
        code: "black",
        img: "./assets/image/crater.png",
      },
      {
        code: "lightgray",
        img: "./assets/image/crater2.png",
      },
    ],
  },
  {
    id: 5,
    title: "Hippie",
    price: 99,
    colors: [
      {
        code: "gray",
        img: "./assets/image/hippie.png",
      },
      {
        code: "black",
        img: "./assets/image/hippie2.png",
      },
    ],
  },
];
// Selector Menu Element 
const wrapper = document.querySelector(".sliderWrapper");
const menuItems = document.querySelectorAll(".menuItem");
// Selector Product Element
const currentProductImg = document.querySelector(".productImg");
const currentProductTitle = document.querySelector(".productTitle");
const currentProductPrice = document.querySelector(".productPrice");
const currentProductColors = document.querySelectorAll(".color");
const currentProductSizes = document.querySelectorAll(".size");
// Modal & Buy,Close Button
const productButton = document.querySelector(".productButton");
const payment = document.querySelector(".payment");
const close = document.querySelector(".close");

let choosenProduct = products[0];
// Handle Event Change Product
menuItems.forEach((item, index) => {
  item.addEventListener("click", () => {
    // change current slide
    wrapper.style.transform = `translateX(${-100 * index}vw)`;
    // change choosen product
    choosenProduct = products[index];
    // change text current product
    currentProductTitle.textContent = choosenProduct.title;
    currentProductPrice.textContent = `$${choosenProduct.price}`;
    currentProductImg.src = choosenProduct.colors[0].img;

    // change background color
    currentProductColors.forEach((color, index) => {
      color.style.backgroundColor = choosenProduct.colors[index].code;
    });
  });
});
// Handle Event Change Product Color
currentProductColors.forEach((color, index) => {
  color.addEventListener("click", () => {
    currentProductImg.src = choosenProduct.colors[index].img;
  });
});
// Handle Event Change Button Color
currentProductSizes.forEach((size, index) => {
  size.addEventListener("click", () => {
    currentProductSizes.forEach((size) => {
      size.style.backgroundColor = "white";
      size.style.color = "black";
    });
    size.style.backgroundColor = "black";
    size.style.color = "white";
  });
});
// Handle Event Open & Close Modal
productButton.addEventListener("click", () => {
  payment.style.display = "flex";
});
close.addEventListener("click", () => {
  payment.style.display = "none";
});

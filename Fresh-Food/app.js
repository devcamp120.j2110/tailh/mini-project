// Go To Top Handle
let goToTopButton = document.querySelector(".go-to-top");
window.onscroll = () => {
  if (
    document.body.scrollTop > 200 ||
    document.documentElement.scrollTop > 200
  ) {
    goToTopButton.style.display = "flex";
  } else {
    goToTopButton.style.display = "none";
  }
};

// Navbar Handle
let menuItems = document.getElementsByClassName("menu-item");

Array.from(menuItems).forEach((item, index) => {
  item.onclick = (e) => {
    // Remove current active
    let currentMenu = document.querySelector(".menu-item.active");
    currentMenu.classList.remove("active");
    // Add Active
    item.classList.add("active");
  };
});

// Food Category
let foodMenuList = document.querySelector(".food-item-wrap");
let foodCategory = document.querySelector(".food-category");
let categorise = document.querySelectorAll("button");

Array.from(categorise).forEach((item, index) => {
  item.onclick = (event) => {
    //   Remove & Add Background Color Button
    let currCate = foodCategory.querySelector("button.active");
    currCate.classList.remove("active");
    event.target.classList.add("active");
    // Add Class data-food-type
    foodMenuList.classList =
      "food-item-wrap " + event.target.getAttribute("data-food-type");
  };
});

// Mobile Nav
let bottomNavItems = document.querySelectorAll(".mb-nav-item");
let bottomMove = document.querySelector(".mb-move-item");

bottomNavItems.forEach((item, index) => {
  item.onclick = (event) => {
    //   Remove Active
    let currentItem = document.querySelector(".mb-nav-item.active");
    currentItem.classList.remove("active");
    //   ADD Active + Change background
    item.classList.add("active");
    bottomMove.style.left = index*25 + "%";
  };
});
